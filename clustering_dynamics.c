#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>

#define buf 1

// MACRO function definitions
#define rnd()  ( (double)rand()/(double)RAND_MAX)

#define sgn(q) ( q>0. ? 1 : q<0. ? -1 : 0) 

#define rand_int(p,q) ( (q>=p) ? ( p+rnd()*(q-p+1) ) : ( q+rnd()*(p-q+1)) )

#define min(p,q) ( (q>=p) ? p : q )

typedef struct {

	struct agent *head; // index of head agent
	struct agent *tail; // index of tail agent
	int size;


} cluster;

typedef struct agent{

	struct agent *next; // link to next agent
	double cap; // capital
	int a; // postion
	double chrc; 
	int active;

} agent;

int count_clusters(cluster *cluster);
int random_cluster_by_size(cluster *cluster);
int random_agent(agent *agent);


// main will initialize clusters of size 1, then coalescence by size occurs for N steps (N defined in Makefile).
int main(int argc, char **argv) {

	int seed = time(NULL);
	srand(seed);

	int steps = atoi(argv[1]);
	agent *agt = (agent*) malloc(N*sizeof(agent));
	cluster *C = (cluster*) malloc(N*sizeof(agent));
	int i;
	double r;

	for(i=0;i<N;i++){
		agt[i].chrc = rnd();
		agt[i].next = NULL; 

		// these two lines for minority game
		r = rnd();
		agt[i].a = sgn(r-.5);
			}	

	int m,j,m_next; // m index of head, j index of cluster
	m = 0;
	m_next = 0;
	j = 0;
	while(m<N){
		m_next += rand_int(1,1);
		if(m_next>N){
			m_next = N;
		}
		C[j].head = &agt[m];
		C[j].tail = &agt[m_next - 1];
		C[j].size = m_next - m;

		for(i=m;i<m_next-1;i++){
			agt[i].next = &agt[i+1];
		}
		agt[m_next-1].next = NULL;
	
		m = m_next;
		j++;
	}
	
	int j1,j2;
	double ch1,ch2;
	double p_co = 0;
	int cluster_count = 0;
	for(i=0;i<steps;i++){
		cluster_count = count_clusters(C);
		//printf("cc = %i\n",cluster_count);
		if(cluster_count==1) break;
		j1 = random_cluster_by_size(C);
		j2 = random_cluster_by_size(C); 
		if(j1==j2){
			i--;
			continue;
		}
		ch1 = C[j1].head->chrc;
		ch2 = C[j2].head->chrc;
		p_co = 1. - fabs(ch1-ch2);
		r = rnd();
		if(r<p_co){
			printf("(C[%p][0].chrc, C[%p][0].chrc, p_co) = (%f, %f, %f) \n",C[j1].head, C[j2].head, ch1, ch2, p_co);
			printf("j1=%i, j2=%i -> (j1+j2).size=(%i)\n",j1,j2,C[j1].size+C[j2].size);
			C[j1].size += C[j2].size;
			C[j2].size = 0;
			C[j1].tail->next = C[j2].head; // join linked lists head to tail
			printf("(%p).next = %p\n\n", C[j1].tail,  C[j2].head);
			C[j1].tail = C[j2].tail;

		
		}
	}
	
	for(i=0;i<N;i++){
		if(C[i].size!=0){ 
			printf("C[%i] = (%p, %p, %i)\n",i, C[i].head, C[i].tail, C[i].size);
			agent *tmp;
			tmp = C[i].head;
			//printf("tmp = (%f)\n",tmp->chrc);
			for(j=0;j<C[i].size;j++){
				printf("C[%i][%i] = (%f, %p, %p)\n",i,j,tmp->chrc,tmp,tmp->next);
				tmp = tmp->next;
			}
			printf("\n");
		}
	}


	return 0;
}

int count_clusters(cluster *cluster){
	int i;
	int res=0;
	for(i=0;i<N*buf;i++){
		if(cluster[i].size>0){
			res++;
		}
	}
	return res;
}

int random_cluster_by_size(cluster *cluster){
	int i;
	double norm = 0.;
	for(i=0;i<N*buf;i++) norm += cluster[i].size;
	double r = rnd();
	double c = 0.;
	for(i=0;i<N*buf;i++){
		c += cluster[i].size/norm;
		if(c>r) break;
	}
	
	return i;
}

int random_agent(agent *agent){
	int i;
	double norm = 0.;
	for(i=0;i<N*buf;i++) norm += 1;
	double r = rnd();
	double c = 0.;
	for(i=0;i<N*buf;i++){
		c += 1./norm;
		if(c>r) break;
	}
	
	return i;
}
