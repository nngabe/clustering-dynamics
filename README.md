# Clustering Dynamics

Cluster and agent data structures for clustering dynamics.

-Agents in a cluster form a linked list (since clusters are of variable size).

-Clusters hold the address of the head and tail agents, making coalescence very simple (tail of one cluster points to head of the other). 

