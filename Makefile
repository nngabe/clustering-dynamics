CC = gcc
CFLAGS = -Wall
DFLAGS = -DN=100
TARGET = clustering_dynamics

default:
	$(CC) $(TARGET).c -o $(TARGET) $(CFLAGS) $(DFLAGS)

clean:
	rm $(TARGET)
